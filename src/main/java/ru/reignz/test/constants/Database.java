package ru.reignz.test.constants;

public interface Database {
    String URL = "jdbc:postgresql://localhost:5432/simple-app-db";
    String LOGIN = "postgres";
    String PASSWORD = "";
}
