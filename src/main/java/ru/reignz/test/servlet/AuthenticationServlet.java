package ru.reignz.test.servlet;

import ru.reignz.test.dao.UserDao;
import ru.reignz.test.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public class AuthenticationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/authentication.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String currentPassword = req.getParameter("password");
        String currentLogin = req.getParameter("login");

        UserDao userDao = new UserDao();
        User currentUser = userDao.getUserByLoginAndPassword(currentLogin, currentPassword);

        if (currentUser != null) {
            req.getSession().setAttribute("user", currentUser);
            resp.sendRedirect("/home");
        } else {
            resp.sendRedirect("/");
        }
    }

}
