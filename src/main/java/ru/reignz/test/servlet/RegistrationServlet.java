package ru.reignz.test.servlet;

import ru.reignz.test.dao.UserDao;
import ru.reignz.test.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/reg")
public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDao userDao = new UserDao();

        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String repeatedPass = req.getParameter("repeatedPass");

        String errorVal = "";

        if (userDao.getUserByLogin(login) != null) {
            //this name already exists
            errorVal = "Пользователь с таким именем уже существует";
            req.getSession().setAttribute("errorVal", errorVal);
            resp.sendRedirect("/reg");
        } else {
            if (password.equals(repeatedPass)) {
                //registered newUser
                User newUser = new User(name, login, password);
                userDao.addUser(newUser);
                req.getSession().setAttribute("user", newUser);
                resp.sendRedirect("/home");

            } else {
                //passwords mismatch
                errorVal = "Пароли не совпадают, попробуйте еще раз";
                req.getSession().setAttribute("errorVal", errorVal);
                resp.sendRedirect("/reg");
            }
        }
    }

}
