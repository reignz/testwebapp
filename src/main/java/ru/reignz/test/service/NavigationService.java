package ru.reignz.test.service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NavigationService {

    public void exit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String exit = req.getParameter("Выход");
        if (exit != null) {
            req.getSession().removeAttribute("user");
            resp.sendRedirect("/");
        }
    }
}
