package ru.reignz.test.dao;

import ru.reignz.test.constants.Database;
import ru.reignz.test.entity.User;

import java.sql.*;
import java.util.ArrayList;

public class UserDao {

    public boolean addUser(User user) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD);) {

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users VALUES (?, ?, ?)");
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getPassword());
            boolean executePreparedStatemant = preparedStatement.execute();
            connection.close();
            return executePreparedStatemant;
        } catch (SQLException e) {
            return false;
        }
    }

    public ArrayList<User> getUserList() {
        ArrayList<User> uList = new ArrayList<User>();
        try {
            Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");

            while (resultSet.next()) {
                String login = resultSet.getString("login");
                String name = resultSet.getString("name");
                String password = resultSet.getString("password");
                uList.add(new User(name, login, password));
            }
            connection.close();
            return uList;
        } catch (SQLException e) {
            return null;
        }
    }

    public User getUserByLogin(String login) {
        try {
            Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD);
            String query = "SELECT * FROM users WHERE login='" + login + "'";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet != null) {
                resultSet.next();
                String name = resultSet.getString("name");
                String password = resultSet.getString("password");
                connection.close();
                return new User(name, login, password);
            } else {
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public User getUserByLoginAndPassword(String login, String password) {
        try {
            Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD);
            String query = "SELECT * FROM users WHERE login='" + login + "' AND password='" + password + "'";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet != null) {
                resultSet.next();
                String name = resultSet.getString("name");
                connection.close();
                return new User(name, login, password);
            } else {
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }
}
