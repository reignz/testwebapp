<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>${name}</title>
</head>
<body>
<div align="center">
    <h1>Хай, ${name}!</h1>

    <div align="right">
        <form action="${pageContext.request.contextPath}/" method="post">
            <input type="submit" value="Выход">
        </form>
    </div>
    <p>
        HOME

</div>
</body>
</html>
